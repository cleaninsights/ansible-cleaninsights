#!/bin/sh
################################################################################
# bootstrap.sh - Bootstrap a new cleaninsights host for management with Ansible.
################################################################################

TARGET_OS=linux
LOGIN_USER=root
INVENTORY_FILE=inventory/production
usage()
{
  echo "Usage: $0 [-u login_user] [-i inventory_file] target"
  exit 2
}

bootstrap_linux()
{
    ssh $LOGIN_USER@$TARGET sudo apt install python3 sudo
    ANSIBLE_DEBUG=false ANSIBLE_VERBOSITY=3 \
    ansible-playbook \
      -i $INVENTORY_FILE \
      --limit $TARGET \
      --user $LOGIN_USER \
      --become-method sudo \
      --become-user root \
      --tags "bootstrap" \
      cleaninsights.cleaninsights.matomo

    echo "Bootstrap complete! The Matomo playbook can now be run: 'ansible-playbook -i inventory/production cleaninsights.cleaninsights.matomo'"
}

while getopts 'u:i:h' c
do
  case $c in
    u) LOGIN_USER=$OPTARG ;;
    i) INVENTORY_FILE=$OPTARG ;;
    h) usage ;;
  esac
done

shift $((OPTIND-1))

TARGET=$1

bootstrap_linux
