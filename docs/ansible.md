<!-- markdownlint-disable -->
## Roles

| Role Name    | Description  |
| ------------ | ------------ |
| [cleaninsights.cleaninsights.ansible-docker-cleaninsights](./roles/ansible-docker-cleaninsights) | Installs the ansible_docker_cleaninsights role |

## Playbooks

| Playbook Name    | Description  |
| ---------------- | ------------ |
| [cleaninsights.cleaninsights.matomo](./playbooks/cleaninsights.cleaninsights.matomo.yml) | Deploy a Clean Insights backend |

<!-- markdownlint-enable -->
