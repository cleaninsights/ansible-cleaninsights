import requests
import time
import sys

now = int(time.time())
print(now)
url = f"https://{sys.argv[1]}/cleaninsights.php"

test_data = {
    "idsite": 1,
    "lang": "en_US",
    "visits": [
        {
            "action_name": "test/project",
            "period_start": now - 25,
            "period_end": now,
            "times": 50,
        }
    ],
    "events": [
        {
            "category": "video-content",
            "action": "start",
            "period_start": now - 25,
            "period_end": now,
            "times": 90,
        }
    ]
}

x = requests.post(url, json=test_data)
print(x)
print(x.text)
