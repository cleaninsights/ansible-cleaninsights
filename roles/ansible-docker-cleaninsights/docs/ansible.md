<!-- markdownlint-disable -->
## Role Variables

| Variable     | Default Value  | Description  |
| ------------ | -------------- | ------------ |
| enable_backupninja | **REQUIRED** | Whether or not to copy backupninja scripts for backing up docker volumes |
| matomo_cleaninsights_token | **REQUIRED** | The Matomo API token used by Cleaninsights to upload metrics |
| matomo_db_name | **REQUIRED** | The Matomo database name. |
| matomo_db_passwords | **REQUIRED** | The MySQL password for the Matomo database. |
| matomo_db_user | **REQUIRED** | The MySQL user for the Matomo database. |
| matomo_domain | **REQUIRED** | The fully qualified domain name for this deployment |
| matomo_letsencrypt_email | **REQUIRED** | The email to use when requesting letsencrypt certificates |
| mysql_image | `mysql:8` | The Docker image to use for the MySQL container. |
| mysql_root_password | **REQUIRED** | The password for the MySQL root user. |

<!-- markdownlint-enable -->
